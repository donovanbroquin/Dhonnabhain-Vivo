package com.dhonnabhain.donovan.vivo;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import android.util.Log;

import java.util.ArrayList;

import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhonnabhain.donovan.vivo.webservice.Album;
import com.dhonnabhain.donovan.vivo.webservice.Artist;
import com.dhonnabhain.donovan.vivo.webservice.Image;
import com.dhonnabhain.donovan.vivo.webservice.WebService;
import com.squareup.picasso.Picasso;

public class ArtistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist);

        final Bundle datas = getIntent().getExtras();
        final ArtistActivity self = this;

        // Modification du titre de la vue
        TextView topLabel = (TextView) findViewById(R.id.topLabel);
        topLabel.setText(datas.getString("artist"));

        // Éouteur pour le bouton back
        ImageView backBut = (ImageView)findViewById(R.id.backBut);
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                self.finish();
            }
        });

        createElement(datas.getString("artistId"), datas.getString("artist"));
    }

    public void createElement(String indexArt, final String artist) {

        // Retour en int pour l'index
        final int index = Integer.parseInt(indexArt);

        // Récupération des informations
        ArrayList<Artist> listeArtistes = WebService.getAll();
        final Artist oneArtist = listeArtistes.get(index);

        // Création des éléments

        // Images de l'artiste
        HorizontalScrollView artistPic = (HorizontalScrollView) findViewById(R.id.artistPic);
        LinearLayout pics = new LinearLayout(this);
        pics.setOrientation(LinearLayout.HORIZONTAL);

        for (int p = 0; p < oneArtist.images.size(); p++) {
            View pictures = getLayoutInflater().inflate(R.layout.picscroll, null);
            ImageView picture = (ImageView) pictures.findViewById(R.id.pic);

            Picasso.with(this.getApplicationContext()).load(oneArtist.images.get(p).link).into(picture);

            pics.addView(pictures);
        }
        artistPic.addView(pics);

        // Styles
        LinearLayout tags = (LinearLayout)findViewById(R.id.artistTags);
        for(int t = 0; t<oneArtist.albums.size(); t++){
            TextView tag = new TextView(this);
            tag.setText(oneArtist.albums.get(t).style);

            tags.addView(tag);
        }

        // Albums
        HorizontalScrollView albumScroll = (HorizontalScrollView) findViewById(R.id.artistAlbum);
        LinearLayout albums = new LinearLayout(this);
        albums.setOrientation(LinearLayout.HORIZONTAL);

        for (int a = 0; a < oneArtist.albums.size(); a++) {

            final String albumIndex = Integer.toString(a);

            Album current = oneArtist.albums.get(a);

            final ArtistActivity self = this;
            final String albumName = current.name;

            View albumScrollView = getLayoutInflater().inflate(R.layout.albumscroll, null);

            ImageView cover = (ImageView) albumScrollView.findViewById(R.id.cover);
            Picasso.with(this.getApplicationContext()).load(current.cover).into(cover);

            cover.setClickable(true);
            cover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(self, AlbumActivity.class);
                    intent.putExtra("albumName", albumName);
                    intent.putExtra("albumIndex", albumIndex);
                    intent.putExtra("artistName", artist);
                    intent.putExtra("artistID", Integer.toString(index));
                    startActivity(intent);
                }
            });

            // Ajout dans la scrollView
            albums.addView(albumScrollView);

            // Concerts
            LinearLayout concertScroll = (LinearLayout) findViewById(R.id.artistConcerts);
            for (int c = 0; c < oneArtist.concerts.size(); c++) {
                final Integer concertIndex = c;
                View concertScrollView = getLayoutInflater().inflate(R.layout.concertscroll, null);

                TextView aConcert = (TextView) concertScrollView.findViewById(R.id.aConcert);
                aConcert.setText(oneArtist.concerts.get(c).place + " - " + oneArtist.concerts.get(c).city);

                TextView aDate = (TextView) concertScrollView.findViewById(R.id.aDate);
                aDate.setText(oneArtist.concerts.get(c).date);

                // Données à passer pour la vue Concert
                /*final String concertCity = oneArtist.concerts.get(c).city;
                String concertPlace = oneArtist.concerts.get(c).place;
                String concertDate = oneArtist.concerts.get(c).date;*/

                concertScrollView.setClickable(true);
                concertScrollView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(self, ConcertActivity.class);
                        intent.putExtra("artistName", artist);
                        intent.putExtra("concertID", concertIndex);
                        intent.putExtra("artistID", index);
                        startActivity(intent);
                    }
                });

                concertScroll.addView(concertScrollView);
            }
        }

        albumScroll.addView(albums);
    }

}
