package com.dhonnabhain.donovan.vivo.webservice;

import android.content.Context;
import android.widget.MediaController;

public class MediaControllerAlwaysVisible extends MediaController
{
    public MediaControllerAlwaysVisible(Context context)
    {
        super(context);
    }

    @Override
    public void show(int timeout)
    {
        super.show(0);
    }
}
