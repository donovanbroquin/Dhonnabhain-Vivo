package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 08/03/2017.
 */

import java.util.ArrayList;

public class Artist {
    public int id;
    public String name;
    public String country;
    public String begin;
    public String end;
    public String label;

    public ArrayList<Album> albums = new ArrayList<Album>();
    public ArrayList<Image> images = new ArrayList<Image>();
    public ArrayList<Concert> concerts = new ArrayList<Concert>();

    public Artist(){}

    public Artist(/*int id,*/ String name, String country)
    {
        //this.id = id;
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString()
    {
        return name;
        //return "\n" + name + "\n\talbums: " + albums + "\n\n";
    }
}
